# A Typical Day 

This game was developed by Diogo Filipe Martins for his Thesis, which was the conclusion of his Masters' Degree at FEUP. The Thesis had a grade of 19/20.

# Hot To Play It 

This game can be played with the following [MacOS executable](https://drive.google.com/drive/folders/1cDwASbBy-CXR5ymAPtvD2vU67ws1ZIea?usp=sharing) or the [Windows executable](https://drive.google.com/drive/folders/1yIAi-spi5272Z1svkXwEEC9Q1nqSFZa7?usp=sharing). 

To play it, the user must download the adequate folder to his computer operating system, and then run the file index.dmg (for MacOS), or index.exe, in some computers presented as index only (for Windows).

# Video Trailer of the Game

[The video trailer of the game can be seen in here.](https://drive.google.com/file/d/1LbaTCxZoUXvJw-gSKXb5_LUahySK_B4g/view)

# Background of the Project

The present work aims to investigate how we can convey and bring consciousness to immigrants’ issues in foreign countries through a technologically gamified approach. The solution was therefore the creation of a role-playing game in first person, allowing the player to stand in the shoes of a character, foreign in a European country, trying to make a living and facing daily inconveniences and challenges that unfortunately exist in reality.

Two focus groups were conducted to gather non-immigrants’ perspectives on immigrants’ experiences, and their user needs, since they compose the target group of the game. Immigrants’ reports of xenophobic situations in Portugal were also collected through surveys created and distributed. After having this information reunited, along with the choice of the game framework, gamification strategies, and game engine, the development of this game started. This phase counted on the game’s structuring, prototyping, scripting, visual design, and implementation. After the development of the game, surveys were created to register its results, and a sample of 45 non-immigrant participants answered them before and after their gaming experience took place. Then, these results were presented, analyzed, and discussed, concluding that the developed game, named "Typical Day", achieved its goal of teaching users and triggering emotion and compassion in them toward immigrants.

# Synopsis, Rules and Goals

This information regarding the game is present right at its beginning, after the players selects "Play".
